---
# Display name
title: Joel Herndon, Ph.D.

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Director, Center for Data and Visualization Sciences

# Organizations/Affiliations to show in About widget
organizations:
- name: Duke University Libraries
  url: https://library.duke.edu/data

# Short bio (displayed in user profile at end of posts)
bio: My research interests include data science, research data services, and data curation and management.

# Interests to show in About widget
interests:
- Data Science
- Research Data Services
- Data Management

# Education to show in About widget
education:
  courses:
  - course: PhD in Comparative Politics
    institution: Emory University
    year: 2001
 

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:joel.herndon@gmail.com'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/jherndon01
  label: Follow me on Twitter
  display:
    header: true
- icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
  icon_pack: fas
  link: https://scholar.google.com/citations?user=d7ZeGH0AAAAJ&hl
- icon: github
  icon_pack: fab
  link: https://github.com/herndonj
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/joel-herndon-4571011a/

# Link to a PDF of your resume/CV from the About widget.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true
---

Joel Herndon leads the Center for Data and Visualization Sciences in Duke University Libraries. The center provides consultations, training, and project support to a variety of data science, data curation, and digital scholarship communities. Joel's recent work focuses on how universities and research libraries are responding to a growing demand for data science services and training.  


