---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Data Curation Network End User Survey 2021"
authors: 
- admin
- Sarah Wright
- Lisa Johnston
- Wanda Marsolek
- Hoa Luong
- Susan Braxton
- Sophia Lafferty-Hess
- Jake Carlson
date: 2021-09-28T13:53:16-05:00
doi: 

# Schedule page publish date (NOT publication's date).
publishDate: 2022-02-24T13:53:16-05:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Data Curation Network End User Survey Data 2021"
publication_short: ""

abstract: "The survey was implemented in Qualtrics and distributed to depositors within 6 Data Curation Network institutions repositories - Cornell University eCommons, Duke University Research Data Repository, Johns Hopkins University Data Archive, Illinois Data Bank, University of Illinois, University of Michigan Deep Blue Data, and the Data Repository for the University of Minnesota (DRUM). The survey population included researchers who have deposited a dataset with repositories at the above institutions between January 1, 2019 to March 15, 2021. The survey ran for about 2 weeks at each institution between April-June 2021. Each survey participant response was linked to a submitted dataset and DOI for the purposes of future analysis and identification. A subset of additional questions were also asked by two institutions, Duke and Michigan, for internal planning assessment purposes and have been included for others to use/reuse. All survey data was de-identified prior to sharing. Each institution completed the IRB process at their respective institutions."

# Summary. An optional shortened abstract.
summary: "This dataset includes the processed dataset from the 2021 End User Survey performed by the Data Curation Network."

tags: [data_curation]
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset: "https://conservancy.umn.edu/handle/11299/224733"
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
