---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Data Sharing Policies in Social Sciences Academic Journals: Evolving Expectations of Data Sharing as a Form of Scholarly Communication"
authors:
- admin
- Robert O'Reilly
date: 2016-07-01T11:25:43-05:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2016-08-01T11:25:43-05:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "In *Databrarianship: The Academic Data Librarian in Theory and Practice Edited by Lynda Kellam and Kristi Thompson*"
publication_short: ""

abstract: "Discussion about data sharing and data availability are widespread in the public and private sectors and within academia. For academic researchers,these discussions often focus on how data sharing and transparency can increase the rigor and credibility of scholarship. In response to these discussions, many academics have argued that academic journals should adopt sharing/replication policies to address these concerns. In this chapter, we analyze whether academic journals in the social sciences have been adopting such policies over time and whether such adoption varies by field. Our conclusion is that while journals are indeed adopting such policies, they are doing so in an incomplete and varied manner. Moreover, the adoption of such policies and the debates that surround them are matters with which libraries should engage themselves, as doing so has the potential to strengthen connections between libraries and researchers with regard to data services and support."

# Summary. An optional shortened abstract.
summary: "A growing number of academic journals have increasingly required data sharing and/or replication policies over the last decade.  This chapter examines this change in light of larger discussions about data sharing and data availability.  We suggest that the growing adoption of data sharing and data management practices presents an opportunity for increased connections and partnerships between libraries and researchers."

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "https://dukespace.lib.duke.edu/dspace/handle/10161/12792"
url_code:
url_dataset: "https://doi.org/10.15139/S3/12157"
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
