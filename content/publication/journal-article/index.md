---
title: "Data Science in the Library"
authors:
- admin
author_notes:
- "Editor"
date: "2022-01-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2021-12-25T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: "*Data Science in the Library*"
publication_short: ""

abstract: Data Science in the Library&colon; Tools and Strategies for Supporting Data-Driven Research and Instruction brings together an international group of librarians and faculty to conside the opportunities afforded by data science for research libraries. Using practical examples, each chapter focuses on data science instruction, reproducible research, establishing data science services and key data science partnerships.

# Summary. An optional shortened abstract.
summary: Data Science in the Library&colon; Tools and Strategies for Supporting Data-Driven Research and Instruction brings together an international group of librarians and faculty to conside the opportunities afforded by data science for research libraries.

tags:
- Source Themes
featured: true

# links:
# - name: ""
#   url: ""
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: 'https://www.facetpublishing.co.uk/page/detail/data-science-in-the-library/?k=9781783304592'
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Facet Publishing**](https://www.facetpublishing.co.uk/page/detail/data-science-in-the-library/?k=9781783304592)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: example
---

<!--{{% callout note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /callout %}}

{{% callout note %}}
Create your slides in Markdown - click the *Slides* button to check out the example.
{{% /callout %}}

Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->

