---
title: A Return to Hugo
author: Joel Herndon
date: '2022-01-16'
slug: Back to Hugo
categories: [infrastructure]
tags: []
subtitle: ''
summary: 'I started using hugo and blogdown roughly five years ago to maintain this site. '
authors: []
lastmod: '2022-01-16T17:21:09-05:00'
featured: yes
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---
I started using the [Hugo static site generator](https://gohugo.io/) in 2017 to maintain my personal website. Yihui Xie's work on integrating blogdown and hugo made it much easier to maintain the site and Yihui Xie, Amber Thomas, and Allison Presmanes Hill's [Creating Websites with R Markdown](https://bookdown.org/yihui/blogdown/) documented how to keep things going in their book . While I create a lot of digital content for my the [Center for Data and Visualization Sciences](https://library.duke.edu/data), I have to admit that I have not kept a close watch on some of the changes taking place with the Hugo Academic Theme! As I tried to update my site to include a new [book](https://www.facetpublishing.co.uk/page/detail/data-science-in-the-library/?k=9781783304592), I discovered that I could no longer build the site without bringing both hugo and the academic theme up to date! Fortunately, the Rstats community had [documented some of the changes](https://www.apreshill.com/blog/2020-12-new-year-new-blogdown/).

While I've seen several posts that suggest it might have been possible to migrate content into the new version of the theme, I've decided to start with a fresh install. I think that this will allow me to review whether to migrate some things forward while introducting some new features (like this post!). I ask for your patience if you are looking for some of the content from the previous site. I hope to have most of the essential pieces restored in the next few days!

